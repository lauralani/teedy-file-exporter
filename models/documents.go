package models

type DocumentItem struct {
	ID              string `json:"id"`
	Highlight       string `json:"highlight"`
	FileID          string `json:"file_id"`
	Title           string `json:"title"`
	Description     string `json:"description"`
	CreateDate      int64  `json:"create_date"`
	UpdateDate      int64  `json:"update_date"`
	Language        string `json:"language"`
	Shared          bool   `json:"shared"`
	ActiveRoute     bool   `json:"active_route"`
	CurrentStepName bool   `json:"current_step_name"`
	FileCount       int    `json:"file_count"`
	Tags            []any  `json:"tags"`
}

type DocumentList struct {
	Total       int            `json:"total"`
	Documents   []DocumentItem `json:"documents"`
	Suggestions []string       `json:"suggestions"`
}
