package models

type FileList struct {
	Files []FileItem `json:"files"`
}

type FileItem struct {
	ID         string `json:"id"`
	Processing bool   `json:"processing"`
	Name       string `json:"name"`
	Version    int    `json:"version"`
	Mimetype   string `json:"mimetype"`
	DocumentID string `json:"document_id"`
	CreateDate int64  `json:"create_date"`
	Size       int    `json:"size"`
}
