# teedy-file-exporter
[![Please don't upload to GitHub](https://nogithub.codeberg.page/badge.svg)](https://nogithub.codeberg.page)

This is a tool to export files from a [Teedy](https://teedy.io) instance.

## Usage
You need [Go 1.21 installed](https://go.dev). And you need to clone this repo =)

Within the cloned repository directory you can now run `go run main.go` and you will be prompted for some infos:

- the Teedy endpoint (format https://dms.example.com WITHOUT trailing slash)
- the Teedy username you want to use for exporting
- the Teedy password for the previously selected user
- the path where the files should be exported to (without a trailing slash)

## License
`teedy-file-exporter` is available under the MIT license. See the LICENSE file for more info.
