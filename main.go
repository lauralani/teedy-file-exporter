package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"

	"codeberg.org/lauralani/teedy-file-exporter/app"
	"codeberg.org/lauralani/teedy-file-exporter/global"
)

func main() {

	reader := bufio.NewReader(os.Stdin)

	fmt.Print("Enter teedy endpoint (format: https://dms.example.com): ")
	readendpoint, _ := reader.ReadString('\n')
	global.Endpoint = strings.Replace(readendpoint, "\n", "", -1)

	global.Cookie = app.GetLoginCookieJar()

	documents := app.GetDocumentList()

	fmt.Print("Enter export directory (format: /var/teedy/export): ")
	readdir, _ := reader.ReadString('\n')
	exportdir := strings.Replace(readdir, "\n", "", -1)

	app.DownloadAllFilesFromAllDocuments(documents, exportdir)

}
