package app

import (
	"bufio"
	"fmt"
	"net/http"
	"net/http/cookiejar"
	"net/url"
	"os"
	"strings"
	"time"

	"log/slog"

	"codeberg.org/lauralani/teedy-file-exporter/global"
)

func GetLoginCookieJar() string {

	jar, err := cookiejar.New(nil)
	if err != nil {
		slog.Error("Can't initialize Cookie Jar", err)
		os.Exit(1)
	}

	reader := bufio.NewReader(os.Stdin)

	fmt.Print("Enter teedy username: ")
	readuser, _ := reader.ReadString('\n')
	username := strings.Replace(readuser, "\n", "", -1)

	fmt.Print("Enter teedy password: ")
	readpass, _ := reader.ReadString('\n')
	password := strings.Replace(readpass, "\n", "", -1)

	client := &http.Client{
		Jar:     jar,
		Timeout: 10 * time.Second,
	}

	data := url.Values{}
	data.Set("username", username)
	data.Set("password", password)

	// Encode the form data
	payload := strings.NewReader(data.Encode())

	request, err := http.NewRequest("POST", global.Endpoint+"/api/user/login", payload)
	if err != nil {
		slog.Error("Can't initialize HTTP Login Request: ", err)
		os.Exit(1)
	}

	request.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	resp, err := client.Do(request)
	if err != nil {
		slog.Error("Error sending request:", err)
		os.Exit(1)
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		slog.Error("Request failed with status code:", resp.Status)
		os.Exit(1)
	}

	cookie := jar.Cookies(resp.Request.URL)[0]

	return cookie.Name + "=" + cookie.Value

}
