package app

import (
	"encoding/json"
	"io"
	"log/slog"
	"net/http"
	"os"
	"time"

	"codeberg.org/lauralani/teedy-file-exporter/global"
	"codeberg.org/lauralani/teedy-file-exporter/models"
)

func GetDocumentList() models.DocumentList {

	url := global.Endpoint + "/api/document/list?limit=0"

	client := &http.Client{Timeout: 10 * time.Second}
	req, err := http.NewRequest("GET", url, nil)

	if err != nil {
		slog.Error("Error creating File List request: ", err)
		os.Exit(1)
	}
	req.Header.Add("Cookie", global.Cookie)

	res, err := client.Do(req)
	if err != nil {
		slog.Error("Error with File List request: ", err)
		os.Exit(1)
	}
	defer res.Body.Close()

	body, err := io.ReadAll(res.Body)
	if err != nil {
		slog.Error("Error reading File List request body: ", err)
		os.Exit(1)
	}

	var documents models.DocumentList

	err = json.Unmarshal(body, &documents)
	if err != nil {
		slog.Error("Error reading JOSN from File List request body: ", err)
		os.Exit(1)
	}

	return documents
}
