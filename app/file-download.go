package app

import (
	"fmt"
	"io"
	"log/slog"
	"net/http"
	"os"
	"strings"

	"codeberg.org/lauralani/teedy-file-exporter/global"
	"codeberg.org/lauralani/teedy-file-exporter/models"
	"github.com/flytam/filenamify"
)

func DownloadAllFilesFromAllDocuments(documents models.DocumentList, exportdir string) {
	for _, doc := range documents.Documents {
		slog.Info("Handling document: ", "Document Name", doc.Title)

		// first we need to get all the files for a document
		files := GetAllfilesFromDocument(doc)
		slog.Info(fmt.Sprintf("  Document has %v files.", len(files)))

		for _, file := range files {
			if file.Name != "" {
				filename, err := filenamify.Filenamify(strings.Split(file.ID, "-")[0]+"_"+file.Name, filenamify.Options{Replacement: "_"})
				if err != nil {
					slog.Error("Error: ", err)
					os.Exit(1)
				}
				filepath := exportdir + "/" + filename

				err = DownloadFile(file.ID, filepath)
				if err != nil {
					slog.Error(err.Error())
					os.Exit(1)
				}
				slog.Info(fmt.Sprintf("  wrote %v", file.Name))
			}
		}

	}

}

func DownloadFile(fileid string, path string) error {
	url := global.Endpoint + "/api/file/" + fileid + "/data"

	client := &http.Client{}
	req, err := http.NewRequest("GET", url, nil)

	if err != nil {
		return fmt.Errorf("can't initialize HTTP Request: %v", err)
	}
	req.Header.Add("Cookie", global.Cookie)

	res, err := client.Do(req)
	if err != nil {
		return fmt.Errorf("error sending request: %v", err)
	}
	defer res.Body.Close()

	outfile, err := os.Create(path)
	if err != nil {
		return fmt.Errorf("path %v is not writeable", path)
	}

	defer outfile.Close()
	_, err = io.Copy(outfile, res.Body)
	if err != nil {
		return fmt.Errorf("can't write to %v", path)
	}

	return nil
}
