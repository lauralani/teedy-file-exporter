package app

import (
	"encoding/json"
	"io"
	"log/slog"
	"net/http"
	"os"

	"codeberg.org/lauralani/teedy-file-exporter/global"
	"codeberg.org/lauralani/teedy-file-exporter/models"
)

func GetAllfilesFromDocument(document models.DocumentItem) []models.FileItem {
	url := global.Endpoint + "/api/file/list?id=" + document.ID

	client := &http.Client{}
	req, err := http.NewRequest("GET", url, nil)

	if err != nil {
		slog.Error("Can't initialize HTTP Request: ", err)
		os.Exit(1)
	}
	req.Header.Add("Cookie", global.Cookie)

	res, err := client.Do(req)
	if err != nil {
		slog.Error("Error sending request:", err)
		os.Exit(1)
	}
	defer res.Body.Close()

	body, err := io.ReadAll(res.Body)
	if err != nil {
		slog.Error("Error reading request body: ", err)
		os.Exit(1)
	}

	var files models.FileList

	err = json.Unmarshal(body, &files)
	if err != nil {
		slog.Error("Error reading JOSN from File List request body: ", err)
		os.Exit(1)
	}

	return files.Files
}
